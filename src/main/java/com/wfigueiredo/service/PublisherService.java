package com.wfigueiredo.service;

import com.wfigueiredo.model.Publisher;
import com.wfigueiredo.repository.PublisherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@Service
public class PublisherService {

    @Autowired
    private PublisherRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    public Publisher create(Publisher publisher) {
        return repository.save(publisher);
    }
    
    public List<Publisher> findAll() {

        Iterable<Publisher> publishers = repository.findAll();
        List<Publisher> publisherList = new ArrayList<>();
        publishers.forEach(publisherList::add);

        return publisherList;
    }

    public Publisher findByName(String name) {

        List<Publisher> publishers = repository.findByNameIgnoreCase(name);

        return !publishers.isEmpty() ? publishers.get(0) : null;
    }

    public Publisher update(Publisher publisher) {
        return repository.save(publisher);
    }

    public boolean exists(String id) { return repository.exists(id); }

    public void delete(String id) { repository.delete(id); }

    public void deleteAll() { repository.deleteAll(); }
}