package com.wfigueiredo.service;

import com.wfigueiredo.model.Console;
import com.wfigueiredo.repository.ConsoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@Service
public class ConsoleService {
    
    @Autowired
    private ConsoleRepository repository;

    public Console create(Console platform) {
        return repository.save(platform);
    }
    
    public List<Console> findAll() {

        Iterable<Console> consoles = repository.findAll();
        List<Console> consoleList = new ArrayList<>();
        consoles.forEach(consoleList::add);

        return consoleList;
    }

    public Console findById(String id) { return repository.findOne(id); }
}
