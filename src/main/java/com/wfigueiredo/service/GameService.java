package com.wfigueiredo.service;

import com.wfigueiredo.model.Game;
import com.wfigueiredo.model.Platform;
import com.wfigueiredo.repository.GameRepository;
import com.wfigueiredo.repository.PlatformRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@Service
public class GameService {

    @Autowired
    private GameRepository repository;

    @Autowired
    private PlatformRepository platformRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    public Game create(Game game) {
        return repository.save(game);
    }

    public List<Game> findAll() {
        Iterable<Game> games = repository.findAll();
        List<Game> gameList = new ArrayList<>();
        games.forEach(gameList::add);
        return gameList;
    }

    public List<Game> findByGenreAndPlatform(String genre, String platformName) {

        Platform platform = platformRepository.findByName(platformName);

        return null;
    }

    public Game findById(String id) { return repository.findOne(id); }
}
