package com.wfigueiredo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;

/**
 * Created by walanem on 06/04/17.
 */
@Document
@Data
@AllArgsConstructor
public class Console {

    @Id
    private String id;

    private String name;

    private Publisher publisher;
}
