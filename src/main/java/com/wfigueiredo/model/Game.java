package com.wfigueiredo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@Document
@Data
@AllArgsConstructor
public class Game {

    @Id
    private String id;

    private String name;

    private String genre;

    private Publisher publisher;

    private List<Console> consoles;
}
