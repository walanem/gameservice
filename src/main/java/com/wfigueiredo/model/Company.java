package com.wfigueiredo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by walanem on 17/04/17.
 */
@Document
@Data
@AllArgsConstructor
public class Company {

    @Id
    private String id;

    private String name;
}