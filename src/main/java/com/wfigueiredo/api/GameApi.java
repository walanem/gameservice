package com.wfigueiredo.api;

import com.wfigueiredo.model.Game;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@Api(value = "game", description = "the game API")
public interface GameApi {

    @ApiOperation(value = "Creates a 'Game'", notes = "", response = Game.class, tags = {"game",})
    @PostMapping(value = "/game",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Game> create(@RequestBody Game game);

    @ApiOperation(value = "List or Find 'Game' objects", notes = "", response = Game.class, responseContainer = "List", tags = {"game",})
    @GetMapping(value = "/game",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<List<Game>> findAll();

    @ApiOperation(value = "Retrieves a 'Game' by Id", notes = "", response = Game.class, tags = {"game",})
    @GetMapping(value = "/game/{gameId}",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Game> findById(@PathVariable("gameId") String gameId);
}
