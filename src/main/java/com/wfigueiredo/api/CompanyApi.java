package com.wfigueiredo.api;

import io.swagger.annotations.Api;

/**
 * Created by walanem on 17/04/17.
 */
@Api(value = "company", description = "the company API")
public interface CompanyApi {
}
