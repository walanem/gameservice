package com.wfigueiredo.api;

import com.wfigueiredo.model.Console;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@Api(value = "console", description = "the console API")
public interface ConsoleApi {

    @ApiOperation(value = "Creates a 'Console'", notes = "", response = Console.class, tags = {"console",})
    @PostMapping(value = "/console",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Console> create(@RequestBody Console console);

    @GetMapping(value = "/console",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<List<Console>> findAll();

    @GetMapping(value = "/console/{consoleId}",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Console> findById(@PathVariable("consoleId") String consoleId);
}
