package com.wfigueiredo.api;

import com.wfigueiredo.model.Publisher;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@Api(value = "publisher", description = "the publisher API")
public interface PublisherApi {

    @ApiOperation(value = "Adds a 'Publisher'", notes = "", response = Publisher.class, tags = {"publisher",})
    @PostMapping(value = "/publisher",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Publisher> create(
            @ApiParam(value = "The 'Publisher' to be created", required = true) @RequestBody Publisher publisher);

    @ApiOperation(value = "List or Find 'Publisher' objects", notes = "", response = Publisher.class, responseContainer = "List", tags = {"publisher",})
    @GetMapping(value = "/publishers",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<List<Publisher>> findAll();

    @ApiOperation(value = "Retrieves a 'Publisher' by name", notes = "", response = Publisher.class, tags = {"publisher",})
    @GetMapping(value = "/publisher/{name}",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Publisher> findByName(
            @ApiParam(value = "Name of the 'Publisher'", required = true) @PathVariable("name") String name);

    @ApiOperation(value = "Updates a 'Publisher'", notes = "", response = Publisher.class, tags = {"publisher",})
    @PutMapping(value = "/publisher/{publisherId}",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Publisher> update(
            @ApiParam(value = "publisherId", required = true) @PathVariable("publisherId") String publisherId,
            @ApiParam(value = "The 'Publisher' to be updated", required = true) @RequestBody Publisher publisher);

    @ApiOperation(value = "Deletes a 'Publisher'", notes = "", response = Void.class, tags = {"publisher",})
    @DeleteMapping(value = "/publisher/{publisherId}",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Void> delete(@PathVariable("publisherId") String publisherId);

    @ApiOperation(value = "Deletes all 'Publisher' objects", notes = "", response = Void.class, tags = {"publisher",})
    @DeleteMapping(value = "/publisher",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Void> deleteAll();
}