package com.wfigueiredo.api;

import io.swagger.annotations.Api;

/**
 * Created by walanem on 17/04/17.
 */
@Api(value = "platform", description = "the platform API")
public interface PlatformApi {
}
