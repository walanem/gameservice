package com.wfigueiredo.repository;

import com.wfigueiredo.model.Publisher;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@Repository("publisherRepository")
public interface PublisherRepository extends MongoRepository<Publisher, String> {

    List<Publisher> findByNameIgnoreCase(String name);
}