package com.wfigueiredo.repository;

import com.wfigueiredo.model.Platform;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by walanem on 06/04/17.
 */
@Repository("PlatformRepository")
public interface PlatformRepository extends MongoRepository<Platform, String> {

    Platform findByName(String name);
}
