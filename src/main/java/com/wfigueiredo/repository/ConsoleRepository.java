package com.wfigueiredo.repository;

import com.wfigueiredo.model.Console;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by walanem on 06/04/17.
 */
@Repository("ConsoleRepository")
public interface ConsoleRepository extends MongoRepository<Console, String> {

    Console findByName(String name);
}
