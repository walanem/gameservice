package com.wfigueiredo.repository;

import com.wfigueiredo.model.Company;
import com.wfigueiredo.model.Platform;
import com.wfigueiredo.model.Publisher;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by walanem on 06/04/17.
 */
@Repository("CompanyRepository")
public interface CompanyRepository extends MongoRepository<Company, String> {

    Publisher findByName(String name);
}
