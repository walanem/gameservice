package com.wfigueiredo.repository;

import com.wfigueiredo.model.Game;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by walanem on 06/04/17.
 */
@Repository("GameRepository")
public interface GameRepository extends MongoRepository<Game, String> {
}
