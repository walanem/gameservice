package com.wfigueiredo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class GameserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameserviceApplication.class, args);
	}
}

@EnableSwagger2
@EnableDiscoveryClient
@Configuration
@EnableHystrix
@EnableHystrixDashboard
class AdditionalConfig {
}