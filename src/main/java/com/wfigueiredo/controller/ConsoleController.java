package com.wfigueiredo.controller;

import com.wfigueiredo.api.ConsoleApi;
import com.wfigueiredo.model.Console;
import com.wfigueiredo.service.ConsoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@RestController
public class ConsoleController implements ConsoleApi {

    @Autowired
    private ConsoleService service;

    public ResponseEntity<Console> create(@RequestBody Console game) {
        return new ResponseEntity<>(service.create(game), HttpStatus.CREATED);
    }

    public ResponseEntity<List<Console>> findAll() {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<Console> findById(@PathVariable("consoleId") String consoleId) {
        return new ResponseEntity<>(service.findById(consoleId), HttpStatus.OK);
    }
}