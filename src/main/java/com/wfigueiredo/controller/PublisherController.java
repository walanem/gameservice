package com.wfigueiredo.controller;

import com.wfigueiredo.api.PublisherApi;
import com.wfigueiredo.model.Publisher;
import com.wfigueiredo.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@RestController
public class PublisherController implements PublisherApi {

    @Autowired
    private PublisherService service;

    public ResponseEntity<Publisher> create(@RequestBody Publisher publisher) {
        return new ResponseEntity<>(service.create(publisher), HttpStatus.CREATED);
    }

    public ResponseEntity<List<Publisher>> findAll() {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<Publisher> findByName(@PathVariable("name") String name) {

        Publisher publisher = service.findByName(name);
        return publisher != null ? new ResponseEntity(publisher, HttpStatus.OK) :
                                   new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<Publisher> update(@PathVariable("publisherId") String publisherId, @RequestBody Publisher publisher) {

        if (this.service.exists(publisherId)) {
            publisher.setId(publisherId);
            return new ResponseEntity<>(this.service.update(publisher), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<Void> delete(@PathVariable("publisherId") String publisherId) {

        if (service.exists(publisherId)) {
            service.delete(publisherId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<Void> deleteAll() {
        service.deleteAll();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}