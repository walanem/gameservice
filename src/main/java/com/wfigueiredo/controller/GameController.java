package com.wfigueiredo.controller;

import com.wfigueiredo.api.GameApi;
import com.wfigueiredo.model.Game;
import com.wfigueiredo.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by walanem on 06/04/17.
 */
@RestController
public class GameController implements GameApi {

    @Autowired
    private GameService service;

    public ResponseEntity<Game> create(@RequestBody Game game) {
        return new ResponseEntity<>(service.create(game), HttpStatus.CREATED);
    }

    public ResponseEntity<List<Game>> findAll() {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    public ResponseEntity<Game> findById(@PathVariable("gameId") String gameId) {
        return new ResponseEntity<>(service.findById(gameId), HttpStatus.OK);
    }
}